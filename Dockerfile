FROM python:3.8

WORKDIR ./app

ENV PYTHONDONTWRITEBYTECODE="1"
#ENV PYTHONUNBUFFERED="1"

RUN apt-get update \
    && apt-get install netcat -y

#RUN apt update && apt install postgresql-dev gcc python3-dev musl-dev

RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY . .

RUN chmod 755 ./entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]
